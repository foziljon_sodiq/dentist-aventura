import Image from "next/image";

export default function Home() {
  return (
    <>
      <header>
        <nav className="py-5 bg-white">
          <div className="mx-auto max-w-4xl flex items-center">
            <Image src={`/public/assets/logo/logo-white.webp`} />
          </div>
        </nav>
      </header>
      <main></main>
      <footer></footer>
    </>
  );
}
